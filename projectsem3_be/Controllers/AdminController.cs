﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using projectsem3_be.Models;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using System.Linq;
using System;
using projectsem3_be.Models.DTOs;

namespace projectsem3_be.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class AdminController : ControllerBase
    {
        private readonly SEM3Team2Context _context;

        public AdminController(SEM3Team2Context context)
        {
            _context = context;
        }

        // GET: api/admin/users
        [HttpGet("users")]
        public async Task<IActionResult> GetUsers()
        {
            try
            {
                // var users = await _context.Users.Include(u => u.UserRoles).ThenInclude(ur => ur.Role).ToListAsync();
                var users = await _context.Users.ToListAsync();
                return Ok(users);
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Internal server error: {ex.Message}");
            }
        }

        // GET: api/admin/users/{id}
        [HttpGet("users/{id}")]
        public async Task<IActionResult> GetUser(int id)
        {
            try
            {
                var user = await _context.Users
                    .Include(u => u.UserRoles)
                        .ThenInclude(ur => ur.Role)
                    .AsNoTracking()
                    .FirstOrDefaultAsync(u => u.UserId == id);

                if (user == null)
                {
                    return NotFound();
                }

                var options = new JsonSerializerOptions
                {
                    ReferenceHandler = ReferenceHandler.Preserve,
                    WriteIndented = true
                };

                return new JsonResult(user, options);
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Internal server error: {ex.Message}");
            }
        }

        // POST: api/admin/users
        [HttpPost("users")]
        public async Task<IActionResult> CreateUser(UserRegisterDTO userDto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                var userExists = await _context.Users.AnyAsync(u => u.Username == userDto.Username);
                if (userExists)
                    return BadRequest("User already exists");

                var user = new User
                {
                    Username = userDto.Username,
                    Email = userDto.Email,
                    Password = BCrypt.Net.BCrypt.HashPassword(userDto.Password)
                };

                _context.Users.Add(user);
                await _context.SaveChangesAsync();

                // Assign default role
                var userRole = new UserRole { UserId = user.UserId, RoleId = 2 };
                _context.UserRoles.Add(userRole);
                await _context.SaveChangesAsync();

                var userResponseDto = new UserResponseDTO
                {
                    UserId = user.UserId,
                    Username = user.Username,
                    Email = user.Email
                };

                return CreatedAtAction(nameof(GetUser), new { id = user.UserId }, userResponseDto);
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Internal server error: {ex.Message}");
            }
        }

        // PUT: api/admin/users/{id}
        [HttpPut("users/{id}")]
        public async Task<IActionResult> UpdateUser(int id, UserDTO userDto)
        {
            if (!ModelState.IsValid)
            {
                // Log the validation errors
                var errors = ModelState.Values.SelectMany(v => v.Errors).Select(e => e.ErrorMessage).ToList();
                return BadRequest(new { Message = "Invalid model state", Errors = errors });
            }

            if (id != userDto.UserId)
            {
                return BadRequest("User ID mismatch");
            }

            try
            {
                var user = await _context.Users
                    .Include(u => u.UserRoles)
                        .ThenInclude(ur => ur.Role)
                    .FirstOrDefaultAsync(u => u.UserId == id);

                if (user == null)
                {
                    return NotFound();
                }

                // Update user properties
                user.Username = userDto.Username;
                user.Email = userDto.Email;

                if (!string.IsNullOrEmpty(userDto.Password))
                {
                    user.Password = BCrypt.Net.BCrypt.HashPassword(userDto.Password);
                }

                // Update roles
                var existingRoles = user.UserRoles.Select(ur => ur.Role.Name).ToList();
                var newRoles = userDto.Roles.Except(existingRoles).ToList();
                var removedRoles = existingRoles.Except(userDto.Roles).ToList();

                foreach (var roleName in newRoles)
                {
                    var role = await _context.Roles.FirstOrDefaultAsync(r => r.Name == roleName);
                    if (role != null)
                    {
                        _context.UserRoles.Add(new UserRole { UserId = user.UserId, RoleId = role.RoleId });
                    }
                }

                foreach (var roleName in removedRoles)
                {
                    var role = await _context.Roles.FirstOrDefaultAsync(r => r.Name == roleName);
                    if (role != null)
                    {
                        var userRole = await _context.UserRoles.FirstOrDefaultAsync(ur => ur.UserId == user.UserId && ur.RoleId == role.RoleId);
                        if (userRole != null)
                        {
                            _context.UserRoles.Remove(userRole);
                        }
                    }
                }

                _context.Entry(user).State = EntityState.Modified;
                await _context.SaveChangesAsync();

                return NoContent();
            }
            catch (Exception ex)
            {
                // Log the exception
                return StatusCode(500, new { Message = "Internal server error", Error = ex.Message });
            }
        }

        // DELETE: api/admin/users/{id}
        [HttpDelete("users/{id}")]
        public async Task<IActionResult> DeleteUser(int id)
        {
            try
            {
                var user = await _context.Users
                    .Include(u => u.UserRoles)
                    .FirstOrDefaultAsync(u => u.UserId == id);

                if (user == null)
                {
                    return NotFound();
                }

                _context.Users.Remove(user);
                await _context.SaveChangesAsync();

                return Ok();
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"Internal server error: {ex.Message}");
            }
        }
    }
}
