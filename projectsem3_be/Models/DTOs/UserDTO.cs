﻿using System.Collections.Generic;

namespace projectsem3_be.Models
{
    public class UserDTO
    {
        public int UserId { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }

        public string Password { get; set; } // Optional
        public List<string> Roles { get; set; }
    }
}
